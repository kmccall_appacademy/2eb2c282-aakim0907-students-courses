class Student
  def initialize(first, last)
    @first_name = first
    @last_name = last
    @courses = []
  end

  def first_name
    @first_name
  end

  def last_name
    @last_name
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def courses
    @courses
  end

  def enroll(course_obj)
    if !@courses.include?(course_obj)
      @courses << course_obj
      course_obj.students << self
      raise "There is a conflict in your schedule." if self.has_conflict?
    end
  end

  def has_conflict?
    conflict = false
    course_pair = @courses.combination(2)
    course_pair.each do |one, two|
      conflict = true if one.conflicts_with?(two)
    end
    conflict
  end

  def course_load
    course_loads = Hash.new(0)
    @courses.each { |c| course_loads[c.department] += c.credits }
    course_loads
  end
end
